The project is uploaded without .env file.

Before you start testing this project, you will have to set properly Laravel 7 and Composer on your computer.

copy .env-example file and rename it in .env

run npm-install in project root folder

Laravel version used: Laravel 7

User Credentials:
	email: admin@example.com
	password: admin
	
DB name: user-activity


Note: 	All requirements are fullified as required in the Task description and no records have been added previously, so you will have to insert activities manually.
	When you first visit any of the routes where records are shown, you will see all records from the logged user. In order to show user activities as required, 
	you will have to insert from date and to date. Then you will get correct filtered records.
	

