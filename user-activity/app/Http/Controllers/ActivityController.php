<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ActivityRequest;
use App\Http\Requests\ActivityReportRequest;
use App\Http\Requests\PrintReportRequest;

use App\Activity;
use App\ActivityObject;

use Yajra\DataTables\DataTables;
use Yajra\DataTables\CollectionDataTable;

class ActivityController extends Controller
{   
    public function activityReport() {

        return view('activity-report');
    }

    public function printReport() {       

        return view('print-report');
    }

    public function showObjects($hash) { 
        
        return view('show-objects');
    }
    
    public function store(ActivityRequest $request) {
        try{
            $input = $request->all();

            $activity = new Activity();

            $activity->date = $input['date'];
            $activity->time = $input['minutes'];
            $activity->description = $input['description'];
            $activity->user_id = \Auth::user()->id;
            $activity->save();

            return response()->json(['activity' => $activity]);
        }catch(Exception $e) {
            return response()->json(['activity' => false]);
        }
    }

    public function activityReportPost(ActivityReportRequest $request) {
        try {
            $input = $request->all();
            
            $id =  \Auth::user()->id;

            $dateFrom = $input['dateFrom'];
            $dateTo = $input['dateTo'];
        
            if($dateFrom && $dateTo) {
                $filter = Activity::where('user_id', $id)->whereBetween('date', [date('Y-m-d',strtotime($dateFrom)), date('Y-m-d', strtotime($dateTo))])
                ->select('id', 'date', 'time', 'description')->orderBy('date', 'asc')->get();
                \Session::put('activitiesObjet', $filter);
                \Session::put('dateFrom', $dateFrom);
                \Session::put('dateTo', $dateTo);    
            } else if ($dateFrom && !$dateTo) {
                $filter = Activity::where('user_id', $id)->where('date', '>=', date('Y-m-d',strtotime($dateFrom)))
                ->select('id', 'date', 'time', 'description')->orderBy('date', 'asc')->get();
                \Session::put('activitiesObjet', $filter);
                \Session::put('dateFrom', $dateFrom);
            } else if (!$dateFrom && $dateTo) {
                $filter = Activity::where('user_id', $id)->where('date', '<=', date('Y-m-d',strtotime($dateTo)))
                ->select('id', 'date', 'time', 'description')->orderBy('date', 'asc')->get();
                \Session::put('activitiesObjet', $filter);
                \Session::put('dateTo', $dateTo);    
            } else {
                $filter = Activity::where('user_id', $id)->select('id', 'date', 'time', 'description')->orderBy('date', 'asc')->get();
                \Session::put('activitiesObjet', $filter);
            }
            
            if($request->ajax()) {
                return DataTables::of($filter)->toJson();
            } else {
                return response()->json(['response' => false]);
            }
        } catch(Exception $e) {
            return response()->json(['response' => false]);
        }
    }        

    public function printReportPost(PrintReportRequest $request) {
        try {

            $input = $request->all();
            $id =  \Auth::user()->id;

            $dateFromTotal = $input['dateFromTotal'];
            $dateToTotal = $input['dateToTotal'];

            if($dateFromTotal && $dateToTotal) {
                $printRecords = Activity::where('user_id', $id)
                ->whereBetween('date', [date('Y-m-d',strtotime($dateFromTotal)), date('Y-m-d', strtotime($dateToTotal))])
                ->select('date',\DB::raw('SUM(time) as total_time'))->groupBy('date')->orderBy('date', 'asc')->get();                  
            } else if ($dateFromTotal && !$dateToTotal) {
                $printRecords = Activity::where('user_id', $id)
                ->where('date', '>=', date('Y-m-d',strtotime($dateFromTotal)))
                ->select('date',\DB::raw('SUM(time) as total_time'))->groupBy('date')->orderBy('date', 'asc')->get();               
            } else if (!$dateFromTotal && $dateToTotal) {
                $printRecords = Activity::where('user_id', $id)
                ->where('date', '>=', date('Y-m-d',strtotime($dateToTotal)))
                ->select('date',\DB::raw('SUM(time) as total_time'))->groupBy('date')->orderBy('date', 'asc')->get();        
            } else {
                $printRecords = Activity::where('user_id', $id)->select('date',\DB::raw('SUM(time) as total_time'))->groupBy('date')->orderBy('date', 'asc')->get();
            }
            
            if($request->ajax()) {
                return DataTables::of($printRecords)->toJson();               
            } else {
                return response()->json(['response' => false]);
            }

        }catch(Exception $e) {
            return response()->json(['response'=> false]);
        }        
    }

    public function requestObjects(Request $request) {

        $input = $request->all();

        $hash = $input['hash'];

        $activity_objects = ActivityObject::where('hash', $hash)->first();
        
        if(!$activity_objects) {
            return redirect('/login');
        }  
        
        $objects = json_decode($activity_objects->activities_data);        

        if($request->ajax()){
            return DataTables::of($objects)->toJson();
        } else {
            return response()->json(['response' => false]);            
        };
    }

    public function edit($id)
    {        
        $activity = Activity::where('id', $id)->first();
        
        if(!$activity) {
            return redirect('/');
        }            

        return view('edit-activity', compact('activity'));        
    }   

    public function update(ActivityRequest $request, $id)
    {   
        try {

            $input = $request->all();
            
            $activity = Activity::where('id', $id)->first();
        
            $activity->date = $input['date'];
            $activity->time = $input['minutes'];
            $activity->description = $input['description'];
            $activity->user_id = \Auth::user()->id;
            $activity->save();

            return response()->json(['response' => $activity]); 
        } catch(Exception $e) {
            return response()->json(['response' => false]); 
        }
    }

    public function destroy($id)
    {   
        try {       
               
            $id = $id;  

            $activity = Activity::find($id);
            $activity->delete($id);      

            return response()->json(['response' => true]);
        } catch(Exception $e) {
            return response()->json(['response' => false]);
        }
    }
}