<?php

namespace App\Http\Controllers;

use Mail;
use App\User;
use App\ActivityObject;
use Illuminate\Http\Request;
use App\Http\Requests\EmailRequest;

use App\Http\Controllers\Controller;

class SendEmailController extends Controller
{
    /**
     * Send an e-mail reminder to the user.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */

    public function send(Request $request)
    {
        try {
            $input = $request->all();

            $validator = \Validator::make($input, [
                'email' => 'required|email'      
            ],
            [
                'email.required' => 'Email field is required.',            
                'email.email' => 'The Send Email field must be a valid email address.'            
            ]);
    
            if ($validator->fails()) {    
                return response()->json($validator->messages(), 200);          
            }

            $activityObject = new ActivityObject();

            $email = $request->post('email');
            
            $items = json_encode(\Session::get('activitiesObjet'));
            $hash = \Str::random(20); 
            $url = request()->getSchemeAndHttpHost().'/show-objects'.'/'.$hash;   

            $activityObject->hash = $hash;
            $activityObject->activities_data = $items;
            $activityObject->save();

            $data = array('email' => $email, 'hash' => $hash, 'url' => $url);
            Mail::send('email-template', $data, function($message) use ($email) {
                $message->to($email)->subject('Activity Report Message');
                $message->from(env('MAIL_TO_ADDRESS'));
            });

            // $session = \Session::flash('emailSent');
            return response()->json(['sent' => true], 200);
        } catch (Exception $e) {
            return response(['sent' => false], 400)->header('Content-Type', 'application/json');
        }
    }
}