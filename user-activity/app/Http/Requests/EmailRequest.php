<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',                
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Send Email field is required.',
            'email.email' => 'The Send Email field must be a valid email address.',
        ];
    }
}
