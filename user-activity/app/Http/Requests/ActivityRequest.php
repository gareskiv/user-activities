<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'minutes' => 'required|integer',
            'description' => 'required|max:255'
        ];
    }

    public function messages() 
    {
        return [
            'date.required'=>'Date field is required.',
            'minutes.required'=>'Time Spent field is required.',
            'description.required'=>'Description field is required.',
            'date.date'=>'Date must be in valid format.',            
        ];
    }


    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->messages(), 200));
    }
}
