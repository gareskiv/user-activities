<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/show-objects/{hash}',          'ActivityController@showObjects')->name('objects');
Route::post('/request-objects',             'ActivityController@requestObjects')->name('request-objects');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home',                     'HomeController@index')->name('home');
    Route::get('/activity-report',          'ActivityController@activityReport')->name('activity-report');
    Route::get('/print-report',             'ActivityController@printReport')->name('print-report');
    Route::get('/edit/{id}',                'ActivityController@edit')->name('edit');   

    Route::post('/store',                   'ActivityController@store')->name('insert');
    Route::post('/activity-report/post',    'ActivityController@activityReportPost')->name('activity-post');
    Route::post('/print-report/post',       'ActivityController@printReportPost')->name('print-post');
    Route::post('/email',                   'SendEmailController@send')->name('sendemail');

    Route::put('/activity-update/{id}',     'ActivityController@update')->name('update');
    Route::delete('/activity/{id}',         'ActivityController@destroy')->name('destroy');

});









