@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-4">
                <div class="card-header">All user activities shown from email url</div>
                <div class="card-body">
                    <div class="scrollableTable">
                        <table class="table table-striped table-borderless" id="showObjectsTable"></table>
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    </div>            
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scriptBlade')
<script>
    $(document).ready(function(){

        let hash = '{{\Request::segment(2)}}';

        // let url = "{{ route('objects', ':hash') }}";
        // url = url.replace(':hash', hash);

        let _token = $('#csrf-token').val();

        let table = $('#showObjectsTable').DataTable({
            'stateSave': true,
            'searching': false,
            'processing': true,
            'serverSide': true,
            'paging': true,
            'bPaginate': true,
            'pagingType': 'simple_numbers',
            'ajax': {
                'url': "{{ route('request-objects') }}",
                'type': 'POST',
                'dataType': "json",
                'data': {
                    '_token': function() { return _token },
                    'hash' : function() { return hash }
                },
                "dataSrc": function (data) {
                    return data.data;
                }
            },
            'columns': [
                {
                    'data': 'id',
                    'name': 'id',
                    'title': 'Activity Id'
                },
                {
                    'data':  function(row) {
                        return moment(row.date).format('MM/DD/YYYY');
                    },
                    'name': 'date',
                    'title': 'Created On'
                },
                {
                    'data': 'time',
                    'name': 'time',
                    'title': 'Duration Time'
                },
                {
                    'data': 'description',
                    'name': 'description',
                    'title': 'Description'
                },
            ],
        });
    });
</script>
@endsection