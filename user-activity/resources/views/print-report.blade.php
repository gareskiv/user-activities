@extends('layouts.app')

@section('content')
<div class="container">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link" href="{{route('home')}}">User Activities</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('activity-report')}}">Activity Report</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="{{route('print-report')}}">Print Report</a>
        </li>
    </ul>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-4">
                <div class="card-header card-header-custom ">{{ __('Print report') }}</div>

                <div class="card-body">
                    <form id="printReport">
                            <div class="form-row">
                            <div class="form-group col-md-5">
                                <label for="dateFromTotal">Date From:</label>
                                <input type="date" class="form-control" id="dateFromTotal" name="dateFromTotal" pattern="\d{1,2}/\d{1,2}/\d{4}">        
                                <div class="text-danger form-group" id="dateFromTotal-error"></div>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="dateToTotal">Date To:</label>
                                <input type="date" class="form-control" id="dateToTotal" name="dateToTotal" pattern="\d{1,2}/\d{1,2}/\d{4}"/>
                                <div class="text-danger form-group" id="dateToTotal-error"></div>
                            </div>
                            <div class="form-group col-md-2">
                                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                <label>&nbsp;</label>
                                <button type="submit" class="btn btn-primary form-control">Search</button>
                            </div>                  
                        </div>      
                    </form>
                </div>
            </div>            
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-4">
                <div class="card-header card-header-custom ">
                    <div>Print activities report</div>
                    <button id="repopulate" class="btn btn-success">Reset</button>
                </div>
                <div class="card-body">
                    <div class="scrollableTable">
                        <table class="table table-striped table-borderless" id="printReportTable"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scriptBlade')
<script>
    $(document).ready(function(){

        let dateFromTotal = null;
        let dateToTotal = null;
        let _token = $('#csrf-token').val();
        
        if(sessionStorage.getItem('button') == 1) {
            $('.pdf-btn').show();
        }

        if (sessionStorage.getItem('dateFromTotal')) {
            $('#dateFromTotal').val(sessionStorage.getItem('dateFromTotal'));
            dateFromTotal = sessionStorage.getItem('dateFromTotal');
        }

        if(sessionStorage.getItem('dateToTotal')) {
            $('#dateToTotal').val(sessionStorage.getItem('dateToTotal'));
            dateToTotal = sessionStorage.getItem('dateToTotal');
        }

        let table = $('#printReportTable').DataTable({
            'dom': 'Bfrtip',
            'stateSave': true,
            'searching': false,
            'processing': true,
            'serverSide': true,
            'paging': true,
            'bPaginate': true,
            'responsive': true,
            'colReorder': true,
            'pagingType': 'simple_numbers',
            'buttons': [
                {   
                    'className': 'btn-danger pdf-btn',
                    'extend': 'pdfHtml5',
                    'text': 'Export PDF',
                    'header': true,
                    'messageTop': `In this document we are showing you the data from User Activity Table in given time interval.`,
                    'customize': function(doc){
                        let colCount = new Array();
                        $('#printReportTable').find('tbody tr:first-child td').each(function(){
                            if($(this).attr('colspan')){
                                for(let i=1;i<=$(this).attr('colspan');$i++){
                                    colCount.push('*');
                                }
                            }else{ colCount.push('*'); }
                        });
                        doc.content[2].table.widths = colCount;
                        doc.styles.title.bold = true,
                        doc.styles.tableBodyEven.alignment = 'center';
                        doc.styles.tableBodyOdd.alignment = 'center'; 
                    },
                    'messageBottom': function () {
                        return 'Total Records: '+ table.rows().count();
                    },
                    'download': true,
                    'pageSize': 'TABLOID',
                    'footer': true,
                }
            ],
            'ajax': {
                'url': "{{ route('print-post') }}",
                'type': 'POST',
                'data': {
                    '_token': function() { return _token },
                    'dateFromTotal': function() { return dateFromTotal },
                    'dateToTotal': function() { return dateToTotal }
                },
                "dataSrc": function (data) {
                    return data.data;
                }
            },
            'columns': [
                {   
                    'data':  function(row) {
                        return moment(row.date).format('MM/DD/YYYY');
                    },
                    'name': 'date',
                    'title': 'Created On' 
                },
                {      
                    'data': 'total_time',
                    "render" : function(data){         
                        return `<div class="total-time-col"> <b>${data}</b> minutes </div>` 
                    },
                    'name': 'total_time',
                    'title': 'Total Time' 
                },
            ],
        });

        if(sessionStorage.getItem('button') != 1) {
            $('.pdf-btn').hide();
        }

        $(document).on('submit', '#printReport', function(e) {
            e.preventDefault();

            table.clear().destroy();

            table = $('#printReportTable').DataTable({
                'dom': 'Bfrtip',
                'stateSave': true,
                'searching': false,
                'processing': true,
                'serverSide': true,
                'paging': true,
                'bPaginate': true,
                'responsive': true,
                'colReorder': true,
                'pagingType': 'simple_numbers',
                'buttons': [
                    {   
                        'className': 'btn-danger pdf-btn',
                        'extend': 'pdfHtml5',
                        'text': 'Export PDF',
                        'header': true,
                        'messageTop': `In this document we are showing you the data from User Activity Table in given time interval.`,
                        'customize': function(doc){
                            let colCount = new Array();
                            $('#printReportTable').find('tbody tr:first-child td').each(function(){
                                if($(this).attr('colspan')){
                                    for(let i=1;i<=$(this).attr('colspan');$i++){
                                        colCount.push('*');
                                    }
                                }else{ colCount.push('*'); }
                            });
                            doc.content[2].table.widths = colCount;
                            doc.styles.title.bold = true,
                            doc.styles.tableBodyEven.alignment = 'center';
                            doc.styles.tableBodyOdd.alignment = 'center'; 
                        },
                        'messageBottom': function () {
                            return 'Total Records: '+ table.rows().count();
                        },
                        'download': true,
                        'pageSize': 'TABLOID',
                        'footer': true,
                    }
                ],
                'ajax': {
                    'url': "{{ route('print-post') }}",
                    'type': 'POST',
                    'data': {
                        '_token': function() { return _token },
                        'dateFromTotal': function() { return $('#dateFromTotal').val() },
                        'dateToTotal': function () {return $('#dateToTotal').val() }
                    },
                    "dataSrc": function (data) {
                        if (data.data === undefined) {
                            $('.pdf-btn').hide();

                            if(data.dateFromTotal) {
                                $('#dateFromTotal-error').show().text(data.dateFromTotal[0]);
                            } else  {
                                $('#dateFromTotal-error').hide('');
                            }

                            if(data.dateToTotal) {
                                $('#dateToTotal-error').show().text(data.dateToTotal[0]);
                            } else  {
                                $('#dateToTotal-error').hide('');
                            }
                            
                            $('#printReportTable_paginate').css('display', 'none');
                            $('#printReportTable_info').css('display', 'none');                           

                            return [];
                        }


                        if($('#dateFromTotal').val() || $('#dateToTotal').val() || $('#dateFromTotal').val() && $('#dateToTotal').val()) {
                            sessionStorage.clear();

                            sessionStorage.setItem('dateFromTotal', $('#dateFromTotal').val());
                            sessionStorage.setItem('dateToTotal', $('#dateToTotal').val());
                            sessionStorage.setItem('button', 1);

                            $('.pdf-btn').show();

                        } else {
                            sessionStorage.clear();

                            $('.pdf-btn').hide();
                        }
                    

                        $('#dateFromTotal-error').hide('');
                        $('#dateToTotal-error').hide('');

                        return data.data;
                    }
                },
                'columns': [
                    {   
                        'data': 'date',
                        'data':  function(row) {
                            return moment(row.date).format('MM/DD/YYYY');
                        },
                        'name': 'date',
                        'title': 'Created On' 
                    },
                    {      
                        'data': 'total_time',
                        "render" : function(data){         
                            return `<div class="total-time-col"> <b>${data}</b> minutes </div>` 
                        },
                        'name': 'total_time',
                        'title': 'Total Time' 
                    },
                ],
            });            
        });        

        $('#repopulate').click(function(e){
            e.preventDefault();
            
            $('.pdf-btn').hide();

            $('#dateFromTotal').val('');
            $('#dateToTotal').val('');

            sessionStorage.clear();
            
            table.ajax.reload();
        });            
    });
</script>
@endsection
