@extends('layouts.app')
@section('content')
<div class="container">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link" href="{{route('home')}}">User Activities</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="{{route('activity-report')}}">Activity Report</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('print-report')}}">Print Report</a>
        </li>
    </ul>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-4">
                <div class="card-header">{{ __('Activity report') }}</div>
                <div class="card-body">
                    <form id="filterActivities">
                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <label for="dateFrom">Date From:</label>
                                <input type="date" class="form-control" id="dateFrom"  name="dateFrom"  value="" pattern="\d{1,2}/\d{1,2}/\d{4}">
                                <div class="text-danger form-group" id="dateFrom-error"></div>          
                            </div>
                            <div class="form-group col-md-5">
                                <label for="dateTo">Date To:</label>
                                <input type="date" class="form-control" id="dateTo"  name="dateTo"  value="" pattern="\d{1,2}/\d{1,2}/\d{4}">
                                <div class="text-danger form-group" id="dateTo-error"></div>     
                            </div>
                            <div class="form-group col-md-2">
                                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                <label>&nbsp;</label>
                                <button type="submit" class="btn btn-primary form-control">Search</button>
                            </div>                  
                        </div>      
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-4">
                <div class="card-header card-header-custom ">
                    <div>All activities report</div>
                    <button id="repopulate" class="btn btn-success">Reset</button>
                </div>
                <div class="card-body">
                    <div class="scrollableTable">
                        <table class="table table-striped table-borderless" id="activityReportTable"></table>
                    </div>
                    <hr>
                    <div class="alert alert-success" id="success-msg" role="alert" style="display:none;">
                        <strong>Email successfully sent.</strong>
                    </div>           
                    <form class="form-inline" id="sendEmail">
                        <div class="form-group mx-sm-3 mb-2">
                            <label for="emai" class="mr-2" >Send Email:</label>
                            <input type="text" class="form-control" id="email"  name="email">         
                        </div>     
                        <div class="form-group col-md-2">
                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                            <button type="submit" class="btn btn-primary mb-2">Send</button>
                        </div>           
                    </form>
                    <div class="text-danger form-group mx-sm-3 mb-2" id="email-error"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scriptBlade')
<script>

    $(document).ready(function(){
        let dateFrom = null;
        let dateTo = null;
        let _token = $('#csrf-token').val();

        if (sessionStorage.getItem('dateFrom')) {
            $('#dateFrom').val(sessionStorage.getItem('dateFrom'));
            dateFrom = sessionStorage.getItem('dateFrom');
        }

        if(sessionStorage.getItem('dateTo')) {
            $('#dateTo').val(sessionStorage.getItem('dateTo'));
            dateTo = sessionStorage.getItem('dateTo');
        }

        let table = $('#activityReportTable').DataTable({
            'stateSave': true,
            'searching': false,
            'processing': true,
            'serverSide': true,
            'paging': true,
            'bPaginate': true,
            'pagingType': 'simple_numbers',
            'ajax': {
                'url': "{{ route('activity-post') }}",
                'type': 'POST',
                'data': {
                    '_token': function() { return _token },
                    'dateFrom': function() { return dateFrom },
                    'dateTo': function() { return dateTo }
                },
                "dataSrc": function (data) {
                    return data.data;
                }
            },
            'columns': [
                {
                    'data': 'id',
                    'name': 'id',
                    'title': 'Activity Id'
                },
                {
                    'data':  function(row) {
                        return moment(row.date).format('MM/DD/YYYY');
                    },
                    'name': 'date',
                    'title': 'Created On'
                },
                {
                    'data': 'time',
                    'name': 'time',
                    'title': 'Duration Time'
                },
                {
                    'data': 'description',
                    'name': 'description',
                    'title': 'Description'

                },
                {   
                    "data" : function(row) {
                        
                        let editId = row.id;
                        let urlEdit = "{{ route('edit', ':id') }}";
                        editUrl = urlEdit.replace(':id', editId);

                        return `<a href="${editUrl}" type="button" class="btn btn-sm btn-primary">Edit</a>` 
                    },
                    'title': 'Edit', 
                },
                {   
                    "data" : function(row){
                        let deleteId = row.id;
                        return ` <form class="form" id="deleteActivity">
                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                            <button type="submit" class="btn btn-sm btn-danger remove" id="${deleteId}">Delete</button>
                        </form>` 
                    },
                    'title': 'Delete', 
                }
            ],
            // 'order': [[ 1, 'asc' ]]
        });

        $(document).on('submit', '#filterActivities', function(e) {
            e.preventDefault();

            table.clear().destroy();

            table = $('#activityReportTable').DataTable({
                'stateSave': true,
                'searching': false,
                'processing': true,
                'serverSide': true,
                'paging': true,
                'pagingType': 'simple_numbers',
                'bPaginate': true,
                'ajax': {
                    'url': "{{ route('activity-post') }}",
                    'type': 'POST',
                    'data': {
                        '_token': function() { return _token },
                        'dateFrom': function() { return $('#dateFrom').val() },
                        'dateTo': function() { return $('#dateTo').val() }
                    },
                    "dataSrc": function (data) {
                        if (data.data === undefined) {

                            if(data.dateFrom) {
                                $('#dateFrom-error').show().text(data.dateFrom[0]);
                            } else  {
                                $('#dateFrom-error').hide('');
                            }

                            if(data.dateTo) {
                                $('#dateTo-error').show().text(data.dateTo[0]);
                            } else  {
                                $('#dateTo-error').hide('');
                            }
                            
                            $('#activityReportTable_paginate').css('display', 'none');
                            $('#activityReportTable_info').css('display', 'none');
                            return [];
                        }

                        sessionStorage.clear();

                        sessionStorage.setItem('dateFrom', $('#dateFrom').val());
                        sessionStorage.setItem('dateTo', $('#dateTo').val());

                        $('#dateFrom-error').hide('');
                        $('#dateTo-error').hide('');

                        return data.data;
                    }
                },
                'columns': [
                    {
                        'data': 'id',
                        'name': 'id',
                        'title': 'Activity Id'
                    },
                    {
                        'data':  function(row) {
                            return moment(row.date).format('MM/DD/YYYY');
                        },
                        'name': 'date',
                        'title': 'Created On'
                    },
                    {
                        'data': 'time',
                        'name': 'time',
                        'title': 'Duration Time'
                    },
                    {
                        'data': 'description',
                        'name': 'description',
                        'title': 'Description'

                    },
                    {   
                        "data" : function(row) {
                            
                            let editId = row.id;
                            let urlEdit = "{{ route('edit', ':id') }}";
                            editUrl = urlEdit.replace(':id', editId);

                            return `<a href="${editUrl}" type="button" class="btn btn-sm btn-primary">Edit</a>` 
                        },
                        'title': 'Edit', 
                    },
                    {   
                        "data" : function(row){
                            let deleteId = row.id;
                            return ` <form class="form" id="deleteActivity">
                                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                <button type="submit" class="btn btn-sm btn-danger remove" id="${deleteId}">Delete</button>
                            </form>` 
                        },
                        'title': 'Delete', 
                    }
                ],
                // 'order': [[ 1, 'asc' ]]
            });
        });

        $(document).on('click', '.remove', function(e) {
            e.preventDefault();

            let id = $(this).attr('id');
            
            let url = "{{ route('destroy', ':id') }}";
            url = url.replace(':id', id);
            
            $.ajaxSetup ({
                cache: false
            });

            $.ajax({
                url: url,
                method:'DELETE',  
                data: {
                    '_token': $('#csrf-token').val(),
                }          
            }).done(function(data){
                if(data) {
                    table.row(id).remove().draw();
                } else {
                    console.log(data);
                }
            });
        });  

        $('#repopulate').click(function(e){
            e.preventDefault();

            $('#dateFrom').val('');
            $('#dateTo').val('');

            sessionStorage.clear();

            table.ajax.reload();
        }); 

        $(document).on('submit', '#sendEmail', function(e) {
            e.preventDefault();
            
            $.ajax({
                url:"{{ route('sendemail') }}",
                method:'POST',
                data: {
                    '_token': $('#csrf-token').val(),
                    'email': $('#email').val(),
                }          
            }).done(function(data){ 
                if(data.sent) {
                    $('#email').val('');
                    $('#success-msg').fadeIn(500);
                    $('#email-error').hide('');
                    setTimeout( function(){$('#success-msg').fadeOut(500)}, 3000);
                }  else {
                    $('#success-msg').fadeOut(500);
                    
                    if(data.email) {
                        $('#email-error').show().text(data.email[0]);
                    } else  {
                        $('#email-error').hide('');
                    }
                }                
            }); 
        });        
    });     
</script>
@endsection
